﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="material.aspx.cs" Inherits="JimmyErickHuamaniMendozaExamen.material" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
    <div class="container" style="width: 640px">
        <h1>Material</h1><hr>
    <form id="form1" runat="server">
               <div class="mb-3">
  <label for="nombreMaterial" class="form-label">Nombre material</label>
&nbsp;<asp:TextBox ID="NombreMaterial" class="form-control" runat="server"></asp:TextBox>
</div>
<div class="mb-3">
  <label for="familiaMaterial" class="form-label">Familia Material</label>

&nbsp;<asp:TextBox ID="FamiliaMaterial" class="form-control" runat="server"></asp:TextBox>
</div>
<div class="mb-3">
  <label for="fechaMaterial" class="form-label">Fecha Material</label>
    <asp:TextBox ID="FechaMaterial" class="form-control" runat="server"></asp:TextBox>

&nbsp;</div>
<div class="mb-3">
  <label for="fechaVencimientoMaterial" class="form-label">Fecha Vencimiento Material</label>

&nbsp;<asp:TextBox ID="FechaVencimientoMaterial" class="form-control" runat="server"></asp:TextBox>
</div>
<div class="mb-3">
  <label for="presentacionMaterial" class="form-label">Presentacion Material</label>
    <asp:TextBox ID="PresentacionMaterial" class="form-control" runat="server"></asp:TextBox>
   
&nbsp;</div>
         <p>
    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" OnClick="Button1_Click" Text="Enviar" />
    </br>
    </form>
        </div>
</div>
</html>
